#include "retangulo.cpp"
#include "triangulo.cpp"
#include "quadrado.cpp"
#include "geometrica.cpp"

#include <iostream>

using namespace std;

int main(){

	retangulo * r = new retangulo(20,20);
	cout << "Calculo da Area " << r->area() << endl;

	triangulo * t = new triangulo(20,20);
	cout << "Calculo da Area " << t->area() << endl;

	quadrado * q = new quadrado(20,20);
	cout << "Calculo da Area " << q->area() << endl;


    return 0;
}