#ifndef RETANGULO_H
#define RETANGULO_H

#include "geometrica.hpp"

class retangulo : public geometrica
{
	public :
		retangulo ();
		retangulo (float base, float altura);

		float area ();
		float area (float altura, float base);
		
};

#endif

 